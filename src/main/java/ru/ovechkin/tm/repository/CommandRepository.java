package ru.ovechkin.tm.repository;

import ru.ovechkin.tm.api.repository.ICommandRepository;
import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.command.auth.*;
import ru.ovechkin.tm.command.project.*;
import ru.ovechkin.tm.command.system.*;
import ru.ovechkin.tm.command.task.*;

import java.util.*;

public class CommandRepository implements ICommandRepository {

    private static final Class[] COMMANDS = new Class[]{
            HelpCommand.class, AboutCommand.class, VersionCommand.class, CommandsAllCommand.class,
            ArgumentsAllCommand.class, SystemInfoCommand.class,

            TaskCreateCommand.class, TaskClearCommand.class,
            TaskListCommand.class, TaskShowByIdCommand.class, TaskShowByIndexCommand.class,
            TaskShowByNameCommand.class, TaskUpdateByIdCommand.class, TaskUpdateByIndexCommand.class,
            TaskRemoveByIdCommand.class, TaskRemoveByIndexCommand.class, TaskRemoveByNameCommand.class,

            ProjectCreateCommand.class, ProjectClearCommand.class, ProjectListCommand.class,
            ProjectShowByIdCommand.class, ProjectShowByIndexCommand.class, ProjectShowByNameCommand.class,
            ProjectUpdateByIdCommand.class, ProjectUpdateByIndexCommand.class, ProjectRemoveByIdCommand.class,
            ProjectRemoveByIndexCommand.class, ProjectRemoveByNameCommand.class,

            ShowProfileCommand.class, RegistryCommand.class, LoginCommand.class, LogoutCommand.class,
            UpdatePasswordCommand.class, UpdateProfileCommand.class,

            UserLockCommand.class, UserUnLockCommand.class, UserRemoveCommand.class,

            ExitCommand.class
    };

    private final List<AbstractCommand> commandList = new ArrayList<>();

    {
        for (final Class clazz : COMMANDS) {
            try {
                final Object commandInstance = clazz.newInstance();
                final AbstractCommand command = (AbstractCommand) commandInstance;
                commandList.add(command);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }


    @Override
    public List<AbstractCommand> getCommandList() {
        return commandList;
    }

}