package ru.ovechkin.tm.repository;

import ru.ovechkin.tm.api.repository.IProjectRepository;
import ru.ovechkin.tm.entity.Project;
import ru.ovechkin.tm.exeption.unknown.IdUnknownException;
import ru.ovechkin.tm.exeption.unknown.IndexUnknownException;
import ru.ovechkin.tm.exeption.unknown.NameUnknownException;

import java.util.ArrayList;
import java.util.List;

public class ProjectRepository implements IProjectRepository {

    private List<Project> projects = new ArrayList<>();

    @Override
    public final void add(final String userId, Project project) {
        project.setUserId(userId);
        projects.add(project);
    }

    @Override
    public final void remove(final String userId, Project project) {
        final List<Project> result = new ArrayList<>();
        for (final Project iterator: projects) {
            if (userId.equals(iterator.getUserId())) result.add(iterator);
        }
        result.remove(project);
    }

    @Override
    public final List<Project> findAll(final String userId) {
        final List<Project> result = new ArrayList<>();
        for (final Project project: projects) {
            if (userId.equals(project.getUserId())) result.add(project);
        }
        return result;
    }

    @Override
    public final void clear(final String userId) {
        final List<Project> projects = findAll(userId);
        this.projects.removeAll(projects);
    }

    @Override
    public Project findById(final String userId, final String id) {
        for (final Project project: projects) {
            if (id.equals(project.getId())) return project;
        }
        throw new IdUnknownException();
    }

    @Override
    public Project findByIndex(final String userId, final Integer index) {
        for (final Project project: projects) {
            if (userId.equals(project.getUserId())) {
                if (projects.indexOf(project) == index) return project;
            }
        }
        throw new IndexUnknownException(index);

    }

    @Override
    public Project findByName(final String userId, final String name) {
        for (final Project project: projects) {
            if (name.equals(project.getName())) return project;
        }
        throw new NameUnknownException();
    }

    @Override
    public Project removeById(final String userId, final String id) {
        final Project project = findById(userId, id);
        if (project == null) throw new IdUnknownException();
        projects.remove(project);
        return project;
    }

    @Override
    public Project removeByIndex(final String userId, final Integer index) {
        final Project project = findByIndex(userId, index);
        if (project == null) return null;
        projects.remove(project);
        return project;
    }

    @Override
    public Project removeByName(final String userId, final String name) {
        final Project project = findByName(userId, name);
        if (project == null) return null;
        projects.remove(project);
        return project;
    }

}