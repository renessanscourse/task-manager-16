package ru.ovechkin.tm.api.service;

import ru.ovechkin.tm.command.AbstractCommand;

import java.util.List;

public interface ICommandService {

    List<AbstractCommand> getCommandList();

}
