package ru.ovechkin.tm.api.repository;

import ru.ovechkin.tm.command.AbstractCommand;

import java.util.List;

public interface ICommandRepository {

    List<AbstractCommand> getCommandList();

}