package ru.ovechkin.tm.constant;

public interface CmdConst {

    String CMD_HELP = "help";

    String CMD_VERSION = "version";

    String CMD_ABOUT = "about";

    String CMD_EXIT = "exit";

    String CMD_INFO = "info";

    String CMD_ARGUMENTS = "arguments";

    String CMD_COMMANDS = "commands";

    String CMD_TASK_LIST = "task-list";

    String CMD_TASK_CLEAR = "task-clear";

    String CMD_TASK_CREATE = "task-create";

    String CMD_PROJECT_LIST = "project-list";

    String CMD_PROJECT_CLEAR = "project-clear";

    String CMD_PROJECT_CREATE = "project-create";

    String TASK_SHOW_BY_ID = "task-show-by-id";

    String TASK_SHOW_BY_INDEX = "task-show-by-index";

    String TASK_SHOW_BY_NAME = "task-show-by-name";

    String TASK_UPDATE_BY_ID = "task-update-by-id";

    String TASK_UPDATE_BY_INDEX = "task-update-by-index";

    String TASK_REMOVE_BY_ID = "task-remove-by-id";

    String TASK_REMOVE_BY_INDEX = "task-remove-by-index";

    String TASK_REMOVE_BY_NAME = "task-remove-by-name";

    String PROJECT_SHOW_BY_ID = "project-show-by-id";

    String PROJECT_SHOW_BY_INDEX = "project-show-by-index";

    String PROJECT_SHOW_BY_NAME = "project-show-by-name";

    String PROJECT_UPDATE_BY_ID = "project-update-by-id";

    String PROJECT_UPDATE_BY_INDEX = "project-update-by-index";

    String PROJECT_REMOVE_BY_ID = "project-remove-by-id";

    String PROJECT_REMOVE_BY_INDEX = "project-remove-by-index";

    String PROJECT_REMOVE_BY_NAME = "project-remove-by-name";

    String LOGIN = "login";

    String LOGOUT = "logout";

    String REGISTRY = "registry";

    String SHOW_PROFILE = "show-profile";

    String UPDATE_PROFILE = "update-profile";

    String UPDATE_PASSWORD = "update-password";

}