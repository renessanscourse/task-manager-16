package ru.ovechkin.tm.command.task;

import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.constant.CmdConst;
import ru.ovechkin.tm.util.TerminalUtil;

public final class TaskClearCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return CmdConst.CMD_TASK_CLEAR;
    }

    @Override
    public String description() {
        return "Remove all tasks";
    }

    @Override
    public void execute() {
        System.out.println("[CLEAR TASKS]");
        final String userId = serviceLocator.getAuthService().getUserId();
        serviceLocator.getTaskService().removeAllTasks(userId);
        System.out.println("[OK]");
    }

}