package ru.ovechkin.tm.command.auth;

import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.constant.CmdConst;
import ru.ovechkin.tm.entity.User;

public class ShowProfileCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return CmdConst.SHOW_PROFILE;
    }

    @Override
    public String description() {
        return "Show information about your account";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        final User user = serviceLocator.getAuthService().findUserByUserId(userId);
        if (user == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[PROFILE INFORMATION]");
        System.out.println("YOUR LOGIN IS: " + user.getLogin());
        System.out.println("YOUR FIRST NAME IS: " + user.getFirstName());
        System.out.println("YOUR MIDDLE NAME IS: " + user.getMiddleName());
        System.out.println("YOUR LAST NAME IS: " + user.getLastName());
        System.out.println("YOUR EMAIL IS: " + user.getEmail());
        System.out.println("YOUR ROLE IS: " + user.getRole());
        System.out.println("[OK]");
    }

}