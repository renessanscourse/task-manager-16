package ru.ovechkin.tm.command.auth;

import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.enumirated.Role;
import ru.ovechkin.tm.util.TerminalUtil;

public class UserLockCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "lock-user";
    }

    @Override
    public String description() {
        return "Lock user's account";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[LOCK USER]");
        System.out.print("ENTER USER'S LOGIN: ");
        final String login = TerminalUtil.nextLine();
        serviceLocator.getUserService().lockUserByLogin(userId, login);
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }

}