package ru.ovechkin.tm.command.auth;

import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.constant.CmdConst;
import ru.ovechkin.tm.exeption.user.AlreadyLoggedInException;
import ru.ovechkin.tm.util.TerminalUtil;

public class LoginCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return CmdConst.LOGIN;
    }

    @Override
    public String description() {
        return "Login in your account";
    }

    @Override
    public void execute() {
        if (serviceLocator.getAuthService().isAuth()) throw new AlreadyLoggedInException();
        System.out.println("[LOGIN]");
        System.out.print("ENTER LOGIN: ");
        final String login = TerminalUtil.nextLine();
        System.out.print("ENTER PASSWORD: ");
        final String password = TerminalUtil.nextLine();
        serviceLocator.getAuthService().login(login, password);
        System.out.println("[OK]");
    }

}