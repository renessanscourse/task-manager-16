package ru.ovechkin.tm.command.project;

import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.constant.CmdConst;
import ru.ovechkin.tm.entity.Project;
import ru.ovechkin.tm.entity.Task;
import ru.ovechkin.tm.util.TerminalUtil;

public final class ProjectRemoveByNameCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return CmdConst.PROJECT_REMOVE_BY_NAME;
    }

    @Override
    public String description() {
        return "Remove project by name";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[REMOVE PROJECT]");
        System.out.print("ENTER PROJECT NAME: ");
        final String name = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().removeProjectByName(userId, name);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[COMPLETE]");
    }

}