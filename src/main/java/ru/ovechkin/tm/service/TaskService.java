package ru.ovechkin.tm.service;

import ru.ovechkin.tm.api.repository.ITaskRepository;
import ru.ovechkin.tm.api.service.ITaskService;
import ru.ovechkin.tm.exeption.empty.IdEmptyException;
import ru.ovechkin.tm.exeption.empty.IndexEmptyException;
import ru.ovechkin.tm.exeption.empty.NameEmptyException;
import ru.ovechkin.tm.entity.Task;
import ru.ovechkin.tm.exeption.empty.UserEmptyException;

import java.util.List;

public class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public void create(final String userId, final String name) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Task task = new Task();
        task.setName(name);
        taskRepository.add(userId, task);
    }

    @Override
    public void create(final String userId, final String name, final String description) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        taskRepository.add(userId, task);
    }

    @Override
    public void add(final String userId, final Task task) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        if (task == null) return;
        taskRepository.add(userId, task);
    }

    @Override
    public void remove(final String userId, final Task task) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        if (task == null) return;
        taskRepository.remove(userId, task);
    }

    @Override
    public List<Task> findAll(final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        return taskRepository.findAll(userId);
    }

    @Override
    public void removeTask(final String userId, final Task task) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        taskRepository.remove(userId, task);
    }

    @Override
    public void removeAllTasks(final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        taskRepository.clear(userId);
    }

    @Override
    public Task findTaskById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return taskRepository.findById(userId, id);
    }

    @Override
    public Task findTaskByIndex(final String userId, final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        if (index == null || index < 0) throw new IndexEmptyException();
        return taskRepository.findByIndex(userId, index);
    }

    @Override
    public Task findTaskByName(final String userId, final String name) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return taskRepository.findByName(userId, name);
    }

    @Override
    public Task updateTaskById(
            final String userId,
            final String id,
            final String name,
            final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Task task = findTaskById(userId, id);
        if (task == null) return null;
        task.setId(id);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateTaskByIndex(
            final String userId,
            final Integer index,
            final String name,
            final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        if (index == null || index < 0) throw new IndexEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Task task = findTaskByIndex(userId, index);
        if (task == null) return null;
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task removeTaskById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return taskRepository.removeById(userId, id);
    }

    @Override
    public Task removeTaskByIndex(final String userId, final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        if (index == null || index < 0) throw new IndexEmptyException();
        return taskRepository.removeByIndex(userId, index);
    }

    @Override
    public Task removeTaskByName(final String userId, final String name) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return taskRepository.removeByName(userId, name);
    }

}