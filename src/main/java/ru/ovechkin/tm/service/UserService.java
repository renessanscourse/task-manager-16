package ru.ovechkin.tm.service;

import ru.ovechkin.tm.api.repository.IUserRepository;
import ru.ovechkin.tm.api.service.IUserService;
import ru.ovechkin.tm.entity.User;
import ru.ovechkin.tm.exeption.empty.*;
import ru.ovechkin.tm.enumirated.Role;
import ru.ovechkin.tm.exeption.unknown.LoginUnknownException;
import ru.ovechkin.tm.exeption.user.AdminBlockingException;
import ru.ovechkin.tm.exeption.user.AdminRemovingException;
import ru.ovechkin.tm.exeption.user.SelfBlockingException;
import ru.ovechkin.tm.exeption.user.SelfRemovingException;
import ru.ovechkin.tm.util.HashUtil;

import java.util.List;

public class UserService implements IUserService {

    private final IUserRepository userRepository;

    public UserService(IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public User findById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return userRepository.findById(id);
    }

    @Override
    public User findByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return userRepository.findByLogin(login);
    }

    @Override
    public User removeUser(final User user) {
        if (user == null) return null;
        return userRepository.removeUser(user);
    }

    @Override
    public User removeById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return userRepository.removeById(id);
    }

    @Override
    public User removeByLogin(final String userId, final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        final User user = findByLogin(login);
        if (user == null) throw new LoginUnknownException();
        final User currentUser = findById(userId);
        if (currentUser == null) return null;
        if (user.getLogin().equals(currentUser.getLogin())) throw new SelfRemovingException();
        if (user.getRole() == Role.ADMIN) throw new AdminRemovingException();
        userRepository.removeByLogin(login);
        return user;
    }

    @Override
    public User create(final String login, final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        return userRepository.add(user);
    }

    @Override
    public User create(final String login, final String password, final String email) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (email == null) throw new EmailEmptyException();
        final User user = create(login, password);
        if (user == null) return null;
        user.setEmail(email);
        return user;
    }

    @Override
    public User create(final String login, final String password, final Role role) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();
        final User user = create(login, password);
        if (user == null) return null;
        user.setRole(role);
        return user;
    }

    @Override
    public User lockUserByLogin(final String userid, final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        final User user = findByLogin(login);
        if (user == null) return null;
        final User currentUser = findById(userid);
        if (currentUser == null) return null;
        if (user.getLogin().equals(currentUser.getLogin())) throw new SelfBlockingException();
        if (user.getRole() == Role.ADMIN) throw new AdminBlockingException();
        user.setLocked(true);
        return user;
    }

    @Override
    public User unLockUserByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        final User user = findByLogin(login);
        if (user == null) return null;
        user.setLocked(false);
        return user;
    }

}